extends Node

# Cursed banget masyaallah

var ready = false

const def_health = 1000
const def_damage = 100
const def_defense = 0

var player_health = 1000
var player_damage = 100
var defense = 0

var coins_prev = 0
var coins = 0

var player = null
var player_active = true

var attack_timer = 0
var defense_timer = 0

var pickup_attack = load("res://Scenes/Object Scenes/Pickup_attack.tscn")
var pickup_health = load("res://Scenes/Object Scenes/Pickup_health.tscn")
var pickup_defense = load("res://Scenes/Object Scenes/Pickup_defense.tscn")

var width = 256
var height = 192

var regen_health = false
var second = 0

func add_health(health):
	if player_health + health >= def_health:
		player_health = def_health
	else:
		player_health += health

func player_damage(damage):
	player_health -= (damage - defense)

func _process(delta):
	if attack_timer > 0:
		attack_timer -= delta
	elif attack_timer <= 0 and player_damage != def_damage:
		player_damage = def_damage
	
	if defense_timer > 0:
		defense_timer -= delta
	elif defense_timer <= 0 and defense != def_defense:
		defense = def_defense
	
	second += delta
	if second >= 1:
		second = 0
		if regen_health:
			add_health(50)

func reset_player_stat():
	coins_prev = coins
	coins = 0
	player_health = def_health
	player_damage = def_damage
	regen_health = false

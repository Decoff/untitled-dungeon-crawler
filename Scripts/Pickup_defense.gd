extends Area2D

export (int) var defense_up = 50
export (int) var timer = 15

func _on_Pickup_attack_body_entered(_body):
	if Global.def_defense + defense_up >= Global.defense:
		Global.defense = Global.def_defense + defense_up
		Global.defense_timer = timer
	queue_free()

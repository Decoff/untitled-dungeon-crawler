extends Control

func _on_StartGame_pressed():
	Global.reset_player_stat()
	get_tree().change_scene("res://Scenes/Object Scenes/Levels/GeneratedLevel.tscn")


func _on_ExitGame_pressed():
	get_tree().quit()

extends Node2D

var pos = Vector2()

func changePos(width, height):
	self.position.x = width * pos.x
	self.position.y = height * pos.y
	#print(str(pos) + ";" + str(self.position))

func removeWall(wall):
	if not wall & 1:
		self.remove_child($TileMap)
	if not wall & 2:
		self.remove_child($TileMap2)
	if not wall & 4:
		self.remove_child($TileMap3)
	if not wall & 8:
		self.remove_child($TileMap4)

extends KinematicBody2D

const def_health = 150
const def_damage = 250

var health = 150
var damage = 250

export (int) var x_speed = 30
export (int) var y_speed = 10
export (int) var max_speed = 20
export (int) var dash_speed = 100

onready var animation = self.get_node("AnimationPlayer")
onready var playerDetection = self.get_node("PlayerDetection")
onready var attackTimer = self.get_node("AttackTimer")
onready var hurtTimer = self.get_node("HurtTimer")
onready var deathTimer = self.get_node("DeathTimer")
onready var healthBar = self.get_node("HealthBar")
onready var healthBar_health = self.get_node("HealthBar/Health")

var velocity = Vector2()
var look_at = Vector2(1, 1)
var player = null

var attacking = false
var dead = false
var hurt = false

func _ready():
	healthBar.hide()
	self.animation.play("Slime_Idle")

func _process(delta):
	velocity = Vector2()
	if health != def_health:
		healthBar.show()
		healthBar_health.rect_size.x = (float(health)/def_health)*16
	player = self.playerDetection.player
	if player:
		velocity = self.global_position.direction_to(player.global_position)
		velocity = velocity.normalized()
		velocity.x = velocity.x * x_speed
		velocity.y = velocity.y * y_speed
	get_move()
	velocity = move_and_slide(velocity, Vector2.UP)

func attack_with_dash():
	if player:
		pass
		
func attack_with_shoot():
	if player:
		pass

func get_move():
	if(dead or hurt):
		velocity = Vector2(0, 0)
	
	if(velocity.x > 0 and look_at != Vector2(1, 1)):
		self.scale = Vector2(1, 1)
		self.rotation_degrees = 0
		look_at = Vector2(1, 1)
	elif(velocity.x < 0 and look_at != Vector2(-1, 1)):
		self.scale = Vector2(1, -1)
		self.rotation_degrees = 180
		look_at = Vector2(-1, 1)
	if(velocity.x != 0 && velocity.y != 0):
		velocity = velocity.normalized() * max_speed


func _on_Hurtbox_area_entered(area):
	if not hurt:
		hurt = true
		self.health -= Global.player_damage
	if not dead and health <= 0:
		dead = true

func _on_DeathTimer_timeout():
	queue_free()

func _on_HurtTimer_timeout():
	hurt = false
	self.animation.play("Slime_Idle")

func _on_AttackDashTimer_timeout():
	attack_dash = true


func _on_AttackShootTimer_timeout():
	pass # Replace with function body.

extends KinematicBody2D

const def_health = 300
const def_damage = 150

var health = 300
var damage = 150

export (int) var x_speed = 50
export (int) var y_speed = 40
export (int) var max_speed = 50

onready var playerDetection = self.get_node("PlayerDetection")
onready var animation = self.get_node("AnimationPlayer")
onready var deathTimer = self.get_node("DeathTimer")
onready var attackTimer = self.get_node("AttackTimer")
onready var hurtTimer = self.get_node("HurtTimer")
onready var healthBar = self.get_node("HealthBar")
onready var healthBar_health = self.get_node("HealthBar/Health")

var velocity = Vector2()
var look_at = Vector2(1, 1)
var bounty = 0

var attacking = false
var dead = false
var hurt = false

func _ready():
	bounty = (randi()%3)+1
	healthBar.hide()
	self.animation.play("Goblin_Idle")

func _physics_process(_delta):
	velocity = Vector2()
	if health != def_health:
		healthBar.show()
		healthBar_health.rect_size.x = (float(health)/def_health)*16
	var player = self.playerDetection.player
	if player_visible(player):
		var dis = self.global_position.distance_to(player.global_position)
		if (dis <= 12) and (not attacking) and (not hurt) and (not dead):
			self.attacking = true
			self.animation.play("Goblin_Attack")
			self.attackTimer.start(0.6)
		velocity = self.global_position.direction_to(player.global_position)
		velocity = velocity.normalized()
		velocity.x = velocity.x * x_speed
		velocity.y = velocity.y * y_speed
	get_move()
	if(velocity.x != 0 or velocity.y != 0):
		self.animation.play("Goblin_Run")
	elif(not attacking and not hurt and not dead):
		self.animation.play("Goblin_Idle")
	velocity = move_and_slide(velocity, Vector2.UP)

func get_move():
	if(attacking or dead or hurt):
		velocity = Vector2(0, 0)
	
	if(velocity.x > 0 and look_at != Vector2(1, 1)):
		self.scale = Vector2(1, 1)
		self.rotation_degrees = 0
		look_at = Vector2(1, 1)
	elif(velocity.x < 0 and look_at != Vector2(-1, 1)):
		self.scale = Vector2(1, -1)
		self.rotation_degrees = 180
		look_at = Vector2(-1, 1)
	if(velocity.x != 0 && velocity.y != 0):
		velocity = velocity.normalized() * max_speed

func player_visible(player):
	# Check player exist
	if player == null:
		return false
	# Check self & player position
	var self_pos_x = floor(self.global_position.x/Global.width)
	var self_pos_y = floor(self.global_position.y/Global.height)
	var player_pos_x = floor(player.global_position.x/Global.width)
	var player_pos_y = floor(player.global_position.y/Global.height)
	if self_pos_x != player_pos_x or self_pos_y != player_pos_y:
		return false
	# Check object between self & player
	var space_state = get_world_2d().direct_space_state
	var raycast = space_state.intersect_ray(global_position, player.global_position, 
		[self], 1, true, false)
	if raycast:
		return false
	return true

func _on_Hurtbox_area_entered(_area):
	var player = self.playerDetection.player
	if not hurt and player_visible(player):
		hurt = true
		self.health -= Global.player_damage
		self.animation.play("Goblin_Hurt")
		self.hurtTimer.start(0.5)
	if not dead and health <= 0:
		dead = true
		self.animation.play("Goblin_Death")
		self.deathTimer.start(0.6)

func _on_DeathTimer_timeout():
	Global.coins += self.bounty
	queue_free()


func _on_AttackTimer_timeout():
	attacking = false
	self.animation.play("Goblin_Idle")


func _on_HurtTimer_timeout():
	hurt = false
	self.animation.play("Goblin_Idle")

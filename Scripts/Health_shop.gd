extends StaticBody2D

onready var playerDetector = self.get_node("PlayerDetection")
onready var healthIcon = self.get_node("HealthIcon")
onready var itemLabel = self.get_node("ItemLabel")

var price = 45

var sold = false
var shown = false

func _ready():
	itemLabel.hide()

func _process(delta):
	var player = playerDetector.player
	if player and not sold:
		if not shown:
			itemLabel.show()
			shown = true
	elif shown:
		itemLabel.hide()
		shown = false
	
	if player and not sold and Input.is_action_just_pressed("interact"):
		if Global.coins >= price:
			Global.coins -= price
			Global.regen_health = true
			healthIcon.hide()
			sold = true

extends StaticBody2D

enum {
	NULL,
	ATTACK,
	HEALTH,
	DEFENSE
}

var item = NULL

func _ready():
	var random = randi()%20
	if random < 3:
		item = ATTACK
	elif random < 6:
		item = HEALTH
	elif random < 9:
		item = DEFENSE

func _on_Area2D_area_entered(_area):
	var item_inst = null
	match item:
		ATTACK:
			item_inst = Global.pickup_attack.instance()
			item_inst.position = self.position + Vector2(8, -6)
			self.get_parent().call_deferred("add_child", item_inst)
		HEALTH:
			item_inst = Global.pickup_health.instance()
			item_inst.position = self.position + Vector2(8, -6)
			self.get_parent().call_deferred("add_child", item_inst)
		DEFENSE:
			item_inst = Global.pickup_defense.instance()
			item_inst.position = self.position + Vector2(8, -6)
			self.get_parent().call_deferred("add_child", item_inst)
	queue_free()

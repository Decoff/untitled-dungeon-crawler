extends StaticBody2D

onready var playerDetector = self.get_node("PlayerDetection")

var coins = 35

var used = false

func _process(delta):
	var player = playerDetector.player
	if player and not used and Input.is_action_just_pressed("interact"):
		Global.coins += coins
		$Chest.frame = 1
		used = true

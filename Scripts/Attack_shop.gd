extends StaticBody2D

onready var playerDetector = self.get_node("PlayerDetection")
onready var attackIcon = self.get_node("AttackIcon")
onready var itemLabel = self.get_node("ItemLabel")

var price = 35

var sold = false
var shown = false

func _ready():
	itemLabel.hide()

func _process(delta):
	var player = playerDetector.player
	if player and not sold:
		if not shown:
			itemLabel.show()
			shown = true
	elif shown:
		itemLabel.hide()
		shown = false
	
	if player and not sold and Input.is_action_just_pressed("interact"):
		if Global.coins >= price and Global.def_damage*2 >= Global.player_damage:
			Global.coins -= price
			Global.player_damage = Global.def_damage*2
			Global.attack_timer = 9000
			attackIcon.hide()
			sold = true

extends Area2D

export (int) var attack_up = 50
export (int) var timer = 12

func _on_Pickup_attack_body_entered(_body):
	if Global.def_damage + attack_up >= Global.player_damage:
		Global.player_damage = Global.def_damage + attack_up
		Global.attack_timer = timer
	queue_free()

extends Area2D

export (int) var hp_restore = 100

func _on_Pickup_health_body_entered(_body):
	Global.add_health(hp_restore)
	queue_free()

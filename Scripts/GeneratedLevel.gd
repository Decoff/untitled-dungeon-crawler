extends Node2D

var player = load("res://Scenes/Object Scenes/Player.tscn")

var room_base = load("res://Scenes/Object Scenes/Rooms/Room_base.tscn")
var room_monster1 = load("res://Scenes/Object Scenes/Rooms/Room_monster1.tscn")
var room_monster2 = load("res://Scenes/Object Scenes/Rooms/Room_monster2.tscn")
var room_box1 = load("res://Scenes/Object Scenes/Rooms/Room_box1.tscn")
var room_treasure = load("res://Scenes/Object Scenes/Rooms/Room_treasure.tscn")
var room_end = load("res://Scenes/Object Scenes/Rooms/Room_end.tscn")
var room_start_2 = load("res://Scenes/Object Scenes/Rooms/Room_start_2.tscn")

onready var playRoom = self.get_node("YSort")
onready var camera = self.get_node("Camera2D")
onready var loadingBG = self.get_node("LoadingBG")
onready var loadingNode = self.get_node("Loading")
onready var loadingTimer = self.get_node("LoadingTimer")
onready var bgm = self.get_node("Camera2D/BGM")

var loading = false

var N = 1
var E = 2
var S = 4
var W = 8

var direction = {Vector2(0, -1) : N, 
				Vector2(1, 0) : E, 
				Vector2(0, 1) : S, 
				Vector2(-1, 0) : W}

var map = {}
var dead_end = []

var size_x = 6
var size_y = 6
var width = Global.width
var height = Global.height

var start_x = 0
var start_y = 0

func search_unvisited(cell, unvisited):
	var result = []
	for dir in direction.keys():
		if cell + dir in unvisited:
			result.append(cell + dir)
	return result

func startGen():
	map = {}
	var unvisited = []
	var stack = []
	dead_end = []
	randomize()
	# Preparation
	for x in size_x:
		for y in size_y:
			#print("initialize cell " + str(Vector2(x, y)))
			map[Vector2(x, y)] = N|E|S|W
			unvisited.append(Vector2(x, y))
	# Start location
	start_x = randi() % (size_x-1)
	start_y = randi() % (size_y)
	#print("Start at " + str(Vector2(start_x, start_y)))
	map[Vector2(start_x, start_y)] = N|S|W
	map[Vector2(start_x+1, start_y)] = N|E|S
	var current = Vector2(start_x+1, start_y)
	unvisited.erase(Vector2(start_x, start_y))
	unvisited.erase(Vector2(start_x+1, start_y))
	stack.append(Vector2(start_x, start_y))
	# Step
	while unvisited:
		var neighbors = search_unvisited(current, unvisited)
		if neighbors:
			var next = neighbors[randi() % neighbors.size()]
			#print(str(next))
			stack.append(current)
			# Remove walls
			var dir = next - current
			var current_walls = map[current] - direction[dir]
			var next_walls = map[next] - direction[-dir]
			# Set walls
			map[current] = current_walls
			map[next] = next_walls
			# Set current
			current = next
			if not search_unvisited(current, unvisited):
				#print("Dead end at " + str(current))
				dead_end.append(current)
			unvisited.erase(current)
		elif stack:
			current = stack.pop_back()
	#print("Map = " + str(map))

func set_room():
	for child in playRoom.get_children():
		playRoom.remove_child(child)
	startGen()
	for cell in map.keys():
		#print("Entered cell " + str(cell))
		var room_wall = room_base.instance()
		room_wall.pos = cell
		room_wall.changePos(width, height)
		room_wall.removeWall(map[cell])
		playRoom.add_child(room_wall)
		if cell != Vector2(start_x, start_y) and not cell in dead_end:
			var random = randi()%20
			var room_inside = null
			if random < 5:
				room_inside = room_monster2.instance()
			elif random < 7:
				room_inside = room_treasure.instance()
			else:
				room_inside = room_monster1.instance()
			room_inside.position = room_wall.position
			playRoom.add_child(room_inside)
		elif cell == Vector2(start_x, start_y):
			var room_inside = room_start_2.instance()
			room_inside.position = room_wall.position
			playRoom.add_child(room_inside)
	for i in dead_end.size():
		if i == 0:
			var room_inside = room_end.instance()
			room_inside.position = Vector2(dead_end[i].x*width, dead_end[i].y*height)
			playRoom.add_child(room_inside)
		else:
			var room_inside = room_box1.instance()
			room_inside.position = Vector2(dead_end[i].x*width, dead_end[i].y*height)
			playRoom.add_child(room_inside)

func start():
	Global.ready = false
	start_loading()
	set_room()
	var player_inst = player.instance()
	player_inst.position = Vector2((start_x*width)+48, (start_y*height)+48)
	playRoom.add_child(player_inst)

func start_loading():
	loading = true
	loadingBG.show()
	loadingNode.show()
	bgm.stop()
	loadingTimer.start(3)

func _ready():
	start()

func _process(_delta):
	if Input.is_action_just_pressed("quit"):
		get_tree().change_scene("res://Scenes/Object Scenes/MainMenu.tscn")
	if loading:
		loadingNode.position = camera.position
	if not Global.player_active:
		Global.player_active = true
		start()


func _on_LoadingTimer_timeout():
	loading = false
	Global.ready = true
	loadingBG.hide()
	loadingNode.hide()
	bgm.play()

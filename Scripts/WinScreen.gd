extends Control

onready var coinLabel = self.get_node("CoinLabel")
onready var anyKeyLabel = self.get_node("AnyKeylabel")

var active = false

func _ready():
	anyKeyLabel.hide()
	coinLabel.text = str(Global.coins)

func _input(event):
	if event is InputEventKey and active:
		get_tree().change_scene("res://Scenes/Object Scenes/MainMenu.tscn")

func _on_Timer_timeout():
	active = true
	anyKeyLabel.show()

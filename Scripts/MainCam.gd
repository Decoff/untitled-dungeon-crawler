extends Camera2D

var x_margin = Global.width
var y_margin = Global.height

onready var hpBar = self.get_node("HealthBar")
onready var attackUpLogo = self.get_node("SwordLogo")
onready var hpRegenLogo = self.get_node("HeartLogo")
onready var defenseLogo = self.get_node("ShieldLogo")
onready var coins = self.get_node("Coins")

var player = null

func _process(_delta):
	player = Global.player
	var size = (float(Global.player_health)/Global.def_health)*70.0
	hpBar.rect_size.x = size
	coins.text = str(Global.coins)
	if Global.attack_timer <= 0:
		attackUpLogo.hide()
	else:
		attackUpLogo.show()
	
	if not Global.regen_health:
		hpRegenLogo.hide()
	else:
		hpRegenLogo.show()
	
	if Global.defense_timer <= 0:
		defenseLogo.hide()
	else:
		defenseLogo.show()
	if player:
		move_cam()

func move_cam():
	var player_x = player.position.x
	var player_y = player.position.y
	if(floor(player_x/x_margin)*x_margin != self.position.x):
		self.position.x = floor(player_x/x_margin)*x_margin
		#print("x = " + str(self.position.x))
	if(floor(player_y/y_margin)*y_margin != self.position.y):
		self.position.y = floor(player_y/y_margin)*y_margin
		#print("y = " + str(self.position.x))

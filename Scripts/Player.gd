extends KinematicBody2D

export (int) var x_speed = 80 #80
export (int) var y_speed = 60 #50
export (int) var max_speed = 80 #60

onready var animation = self.get_node("AnimationPlayer")
onready var attackTimer = self.get_node("AttackTimer")
onready var deathTimer = self.get_node("DeathTimer")
onready var hurtTimer = self.get_node("HurtTimer")
onready var hitbox = self.get_node("Hitbox")

var defHitboxPos = Vector2()

var velocity = Vector2()
var look_at = Vector2()

var attacking = false
var dead = false
var hurt = false

func _ready():
	Global.player = self
	defHitboxPos = hitbox.position
	look_at = Vector2(1, 1)
	animation.play("Player1_Idle")

func _physics_process(delta):
	velocity = Vector2()
	move(delta)

func _process(_delta):
	if(Global.ready and Input.is_action_just_pressed("attack") and (not attacking)):
		attacking = true
		animation.play("Player1_Attack")
		attackTimer.start(0.5)

func move(_delta):
	if(Global.ready):
		get_input()
	if(velocity.x > 0 and look_at != Vector2(1, 1)):
		self.scale = Vector2(1, 1)
		self.rotation_degrees = 0
		look_at = Vector2(1, 1)
	elif(velocity.x < 0 and look_at != Vector2(-1, 1)):
		self.scale = Vector2(1, -1)
		self.rotation_degrees = 180
		look_at = Vector2(-1, 1)
	if(velocity.x != 0 and velocity.y != 0):
		velocity = velocity.normalized() * max_speed
	if(velocity.x != 0 or velocity.y != 0):
		animation.play("Player1_Run")
	elif(not attacking and not hurt and not dead):
		animation.play("Player1_Idle")
	velocity = move_and_slide(velocity, Vector2.UP)

func get_input():
	if(Input.is_action_pressed("down")):
		velocity.y += y_speed
		hitbox.position = Vector2(defHitboxPos.y, defHitboxPos.x)
		hitbox.rotation_degrees = 90
	if(Input.is_action_pressed("up")):
		velocity.y -= y_speed
		hitbox.position = Vector2(defHitboxPos.y, -defHitboxPos.x)
		hitbox.rotation_degrees = 90
	if(Input.is_action_pressed("right")):
		velocity.x += x_speed
		hitbox.position = defHitboxPos
		hitbox.rotation_degrees = 0
	if(Input.is_action_pressed("left")):
		velocity.x -= x_speed
		hitbox.position = defHitboxPos
		hitbox.rotation_degrees = 0
	if(attacking or dead):
		velocity = Vector2(0, 0)
	

func _on_AttackTimer_timeout():
	attacking = false
	animation.play("Player1_Idle")


func _on_Hurtbox_area_entered(area):
	if not hurt:
		hurt = true
		Global.player_damage(area.get_parent().damage)
		self.animation.play("Player1_Hurt")
		self.hurtTimer.start(0.5)
	if not dead and Global.player_health <= 0:
		dead = true
		self.animation.play("Player1_Death")
		self.deathTimer.start(0.8)

func _on_DeathTimer_timeout():
	Global.player_active = false
	Global.reset_player_stat()
	queue_free()


func _on_HurtTimer_timeout():
	hurt = false
	self.animation.play("Player1_Idle")
